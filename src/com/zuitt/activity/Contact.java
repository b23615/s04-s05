package com.zuitt.activity;

public class Contact {
    // 3. Declare the ff. instance variables/ properties for the Contact class:
      // name of data type String, contactNumber of data type String and address of data type String.
    private String name;
    private String contactNumber;
    private String address;

    // 4. Define a default constructor for the Contact class.
    public Contact(){}

    // 4. Define a parameterized constructor for the Contact class.
    public Contact(String name, String contactNumber, String address){
        this.name = name;
        this.contactNumber = contactNumber;
        this.address = address;
    }
     // 5. Getter
        public String getName(){
            return this.name;
        }
        public String getContactNumber(){
            return this.contactNumber;
        }
        public String getAddress(){
            return this.address;
        }
    // 5. Setter
        public void setName(String name){
        this.name = name;
        }
        public void setContactNumber(String contactNumber){
        this.contactNumber = contactNumber;;
        }
        public void setAddress(String address){
            this.address = address;
        }

        public void printContact(){
            System.out.println(this.name);
            System.out.println(this.name + " has the following registered number: " + this.contactNumber);
            System.out.println(this.name + " has the following registered address: my home in " + this.address);
        }
}
