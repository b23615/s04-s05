package com.zuitt.activity;

import java.util.ArrayList;

public class Phonebook {
    // 6. Create a single instance variable/property.
    private ArrayList<Contact> contacts = new ArrayList<>();
    // 7. Default constructor of Phonebook class.
    public Phonebook(){}

    // 7. Parameterized constructor Phonebook class.
    public Phonebook(ArrayList contacts) {
        this.contacts = contacts;
    }
    // 8. Getter
    public ArrayList getContacts() {
        return contacts;
    }
    // 8. Setter , 11. add both contacts
    public void setContacts(Contact contact) {
        this.contacts.add(contact) ;
    }
}
