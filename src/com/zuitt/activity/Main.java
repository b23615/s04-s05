package com.zuitt.activity;

import com.zuitt.example.Car;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {

        // 10. For first contact
        Contact firstContact = new Contact();

        // 10. setter
        firstContact.setName("John Doe");
        firstContact.setContactNumber("+639152468596");
        firstContact.setAddress("Quezon City");

        // 10. For second contact
        Contact secondContact = new Contact();

        // 10. setter
        secondContact.setName("Jane Doe");
        secondContact.setContactNumber("+639162148573");
        secondContact.setAddress("Caloocan City");

        // 9. Create a Phonebook
        Phonebook myPhonebook = new Phonebook();
        myPhonebook.setContacts(firstContact);
        myPhonebook.setContacts(secondContact);

        // 12. Control structure
        if (myPhonebook.getContacts().isEmpty()){
            System.out.println("Phonebook is empty. Please add a contact.");
        } else {
            ArrayList<Contact> contacts = myPhonebook.getContacts();

            for(Contact contact : contacts){
                contact.printContact();
            }
        }

    }
}

