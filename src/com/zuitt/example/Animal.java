package com.zuitt.example;

// Parent Class
public class Animal {
    // properties / fields
    private String name;
    private String color;

    // constructor
    // empty constructor
     public Animal(){};

    // parameterized constructor
        public Animal(String name, String color){
            this.name = name;
            this.color = color;
        }

    // getter and setter
    // read only, write only or read and write
        public String getName() {
            return this.name;
        }

        public String getColor() {
            return this.color;
        }

        public void setName(String name) {
            this.name = name;
        }

        public void setColor(String color) {
            this.color = color;
        }

    // method
        public void call(){
            System.out.println("Hi my name is " + this.name);
        }
}
